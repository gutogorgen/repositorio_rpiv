/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matheus
 */
public class CaronistaTest {
    
    public CaronistaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of incluirCaronista method, of class Caronista.
     */
    @Test
    public void testIncluirCaronista() throws Exception {
        
        System.out.println("incluirCaronista");
        String nomeCompleto = "nome";
        String nomeUser = "nome";
        String matricula = "0909";
        String senha = "j&7hHDG";
        String email = "matheus@matheus.com.be";
        int campusOrigem = 1;
        String dataNascimento = "24/10/1990";
        String cpf = "47347422353";
        boolean expResult = false; // executado a primeira vez true, mais de uma vez com os mesmos dados false
        boolean result = Caronista.incluirCaronista(nomeCompleto, nomeUser, matricula, senha, email, campusOrigem, dataNascimento, cpf);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }
    
    /**
//     * Test of alterarCaronista method, of class Caronista.
//     */
//    @Test
    
     public void testAlterarCaronista() throws Exception {
         
         //alterarCaronista("matheus de oliveira marinho","matheus", "101010101010101010101", "@s1Acccc", "joao@hotamil.com.br",1, "20/10/80", user.getIdusuario());
         Login login = new Login();
        Usuario user;
        user = login.verificaAutorização("@s1Acccc", "matheus");
        System.out.println("alterarCaronista");
        String nome = "matheus de oliveira marinho";
        String nomeUser = "matheus";
        String matricula = "5678945678";
        String senha = "@s1Acccc";
        String email = "contato@matheusmarinho.com.br";
        int campusOrigem = 1;
        String dataNascimento = "24/10/1990";
        int idUsuario = user.getIdusuario();
        boolean expResult = true;
        boolean result = Caronista.alterarCaronista(nome, nomeUser, matricula, senha, email, campusOrigem, dataNascimento, idUsuario);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
}
     /**
//     * Test of verficaUsuarioVálido method, of class Caronista.
     */
    @Test
    public void testVerficaUsuarioVálido() throws SQLException {
        System.out.println("verficaUsuarioVálido");
        Login login = new Login();
        Usuario user;
        user = login.verificaAutorização("@s1Acccc", "matheus");
        int usuarioId = user.getIdusuario();
        boolean expResult = true;
        boolean result = Caronista.verficaUsuarioVálido(usuarioId).next();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }
     /**
     * Test of listarCaronista method, of class Caronista.
     */
    @Test
    public void testListarCaronista() throws Exception {
        
        System.out.println("listarCaronista");
        System.out.println("verficaUsuarioVálido");
        Login login = new Login();
        Usuario user;
        user = login.verificaAutorização("@s1Acccc", "matheus");
        int campoOrigem = user.getCampusOrigem();
        boolean expResult = true;
        boolean result = Caronista.listarCaronista(campoOrigem);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
    /**
     * Test of validaatricula method, of class Caronista.
     */
    @Test
    public void testValidaatricula() {
        System.out.println("validaatricula");
        String texto = "1234567";
        boolean expResult = true;
        boolean result = Caronista.validaatricula(texto);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
    /**
     * Test of buscarCarona method, of class Caronista.
     */
    @Test
    public void testBuscarCarona() {
        System.out.println("buscarCarona");
        String data = "";
        String hora = "";
        Caronista instance = new Caronista();
        instance.buscarCarona(data, hora);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    


}

//
//    /**
//     * Test of alterarHorarioViagemExistente method, of class Caronista.
//     */
//    @Test
//    public void testAlterarHorarioViagemExistente() {
//        System.out.println("alterarHorarioViagemExistente");
//        Caronista instance = new Caronista();
//        boolean expResult = false;
//        boolean result = instance.alterarHorarioViagemExistente();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }