/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBviagemFretada;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GuTo
 */
public class ViagemFretadaTest {

    public ViagemFretadaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of inserirViagemFretada method, of class ViagemFretada.
     */
//    @Test
//    public void testInserirViagemFretada() {
//        //TESTA INSERSAO DE VIAGEM FRETADA SEM PARAMETROS PELA MODEL
//
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        boolean result = instance.inserirViagemFretada();
//
//        assertEquals(expResult, result);
//
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        assertNotSame(new Integer(1), fretada.size());
//        
//        
//
//        
//    }

    @Test
    public void testInserirViagemFretada2() {
        //TESTA INSERSAO DE VIAGEM FRETADA COM PARAMETROS CORRETOS
        System.out.println("inserirViagemFretada");
        ViagemFretada instance = new ViagemFretada();

        instance.deletarTodasViagensFretadas();

        boolean expResult = true;
        instance.setCpfresponsavel("02732234028");//cpf valido
        instance.setDatachegada("20130408");//data de chegada valida 
        instance.setDatasaida("20130407");//data de partida valida
        instance.setDestino("sao francisco");//destino valido
        instance.setHorachegada("2312");//hora de chegada valida
        instance.setHorasaida("1221");//hora de saida valida
        instance.setIdcampusorigem(1);//campus valido
        //instance.setIdveiculo(1);//veiculo valido
        instance.setPlaca("abc123");
        instance.setIdmotorista(1);//motorista valido
        instance.setNumeropassageiros(3);//numero de passageiros valido
        instance.setResponsavel("joao");//nome de responsavel valido

        boolean result = instance.inserirViagemFretada();
//        System.out.println(instance.getResponsavel());

        assertEquals(expResult, result);

        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
        DBviagemFretada dbfretada = new DBviagemFretada();
        dbfretada.listar(fretada);
        
        
        if (fretada.size() == 1) {
            System.out.println("Cadastro realizado");
            

        } else {
            System.out.println("Ocorreu Algum problema");
            assertEquals(expResult, result);

        }
    }
//
//    @Test
//    public void testInserirViagemFretada3() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM DATA DE RETORNO INFERIOR A DATA DE PARTIDA 
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//
//        instance.deletarTodasViagensFretadas();
//
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("20130406");//data de chegada INVALIDA 
//        instance.setDatasaida("20130407");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(1);//campus valido
//        //instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if (fretada.size() == 0) {
//            System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//            assertEquals(expResult, result);
//        } else {
//            System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//            assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada4() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM DATA DE CHEGADA 
//        //DATA DE PARTIDA HORA CHEGADA E HORA PARTIDA CONTENDO LETRAS  
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("B0130A0O");//data de chegada INVALIDA 
//        instance.setDatasaida("2C01C3AA");//data de partida INVALIDA
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("23BV");//hora de chegada INVALIDA
//        instance.setHorasaida("ABCD");//hora de saida INVALIDA
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if (fretada.size() == 0) {
//            System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//            assertEquals(expResult, result);
//        } else {
//            System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//            assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada5() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM DATA DE RETORNO DATA DE PARTDIA INFERIOR A 8 DIGITOS
//        //HORA DE RETORNO E HORA DE PARTIDA INFERIOR A 4 DIGITOS
//
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("2013");//data de chegada INVALIDA 
//        instance.setDatasaida("20107");//data de partida INVALIDA
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("12");//hora de chegada INVALIDA
//        instance.setHorasaida("121");//hora de saida INVALIDA
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada6() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM DATA DE RETORNO E DATA DE PARTDIA SUPERIOR A 8 DIGITOS
//        //HORA DE RETORNO E HORA DE PARTIDA SUPERIOR A 4 DIGITOS
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("201304069");//data de chegada INVALIDA 
//        instance.setDatasaida("201304075");//data de partida INVALIDA
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("231276");//hora de chegada INVALIDA
//        instance.setHorasaida("12215");//hora de saida INVALIDA
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada7() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM CAMPUS INVALIDO
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("20130419");//data de chegada valida 
//        instance.setDatasaida("20130417");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(10);//campus INVALIDO
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//       ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada8() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM VEICULO INVALIDO
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("20130419");//data de chegada valida 
//        instance.setDatasaida("20130417");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(5);//veiculo INVALIDO
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//       ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada9() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM MOTORISTA INVALIDO
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("20130419");//data de chegada valida 
//        instance.setDatasaida("20130417");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(9);//motorista INVALIDO
//        instance.setNumeropassageiros(3);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada10() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM NUMERO DE PASSAGEIROS INVALIDO
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234028");//cpf valido
//        instance.setDatachegada("20130419");//data de chegada valida 
//        instance.setDatasaida("20130417");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(30);//numero de passageiros INVALIDO
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }
//
//    @Test
//    public void testInserirViagemFretada11() {
//
//        //TESTA INSERSAO DE VIAGEM FRETADA COM CPF DO RESPONSAVEL INVALIDO
//        System.out.println("inserirViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.deletarTodasViagensFretadas();
//        boolean expResult = false;
//        instance.setCpfresponsavel("02732234027");//cpf INVALIDO
//        instance.setDatachegada("20130419");//data de chegada valida 
//        instance.setDatasaida("20130417");//data de partida valida
//        instance.setDestino("sao francisco");//destino valido
//        instance.setHorachegada("2312");//hora de chegada valida
//        instance.setHorasaida("1221");//hora de saida valida
//        instance.setIdcampusorigem(1);//campus valido
//        instance.setIdveiculo(1);//veiculo valido
//        instance.setIdmotorista(1);//motorista valido
//        instance.setNumeropassageiros(30);//numero de passageiros valido
//        instance.setResponsavel("joao");//nome de responsavel valido
//
//        boolean result = instance.inserirViagemFretada();
//       ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
//        DBviagemFretada dbfretada = new DBviagemFretada();
//        dbfretada.listar(fretada);
//        if(fretada.size() == 0){
//          System.out.println("Teste Aprovado --- Não foi inserido nem um dado no banco!");
//          assertEquals(expResult, result);
//        }else{
//          System.out.println("Teste Reprovado --- foi inserido dados no banco!");
//          assertEquals(expResult, result);
//        }
//    }

    /**
     * Test of mostrarViagemFretada method, of class ViagemFretada.
     */
//    @Test
//    public void testMostrarViagemFretada() {
//        //VERIFICA SE ESTA IMPRIMINDO VIAGENS FRETADAS JA CADASTRADAS
//        System.out.println("mostrarViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        boolean expResult = true;
//        boolean result = instance.mostrarViagemFretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //fail("The test case is a prototype.");
//    }
//
//    //
////    /**
////     * Test of deletarViagemFretada method, of class ViagemFretada.
////     */
//    @Test
//    public void testDeletarViagemFretada() {
//        System.out.println("deletarViagemFretada");
//        ViagemFretada viagemfretada = null;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setIdviagemfretada(6);
//        boolean expResult = false;
//        boolean result = instance.deletarViagemFretada(instance);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //    fail("The test case is a prototype.");
//    }
//
////    /**
////     * Test of alterarViagemFretada method, of class ViagemFretada.
////     */
//    @Test
//    public void testAlterarViagemFretada() {
//        //TESTA ALTERAR VIAGEM FRETADA SEM PASSAR PARAMETROS
//        System.out.println("alterarViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        boolean expResult = false;
//        boolean result = instance.alterarViagemFretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //  fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testAlterarViagemFretada2() {
//        //TESTA ALTERAR VAIGEM FRETADA COM PARAMETROS INCORRETOS
//        System.out.println("alterarViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.setCpfresponsavel("");
//        instance.setDatachegada("");
//        instance.setDatasaida("");
//        instance.setDestino("");
//        instance.setHorachegada("");
//        instance.setHorasaida("");
//        instance.setIdcampusorigem(0);
//        instance.setIdmotorista(0);
//        instance.setIdveiculo(0);
//        instance.setIdviagemfretada(0);
//        instance.setNumeropassageiros(0);
//        instance.setResponsavel("");
//        boolean expResult = false;
//        boolean result = instance.alterarViagemFretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //  fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testAlterarViagemFretada3() {
//        //TESTA ALTERAR VIAGEM FRETADA COM PARAMETROS CORRETOS
//        System.out.println("alterarViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.setCpfresponsavel("02732234028"); // VALIDO
//        instance.setDatachegada("20140207"); //VALIDO
//        instance.setDatasaida("20140201"); //VALIDO
//        instance.setDestino("Bolivia");   //VALIDO
//        instance.setHorachegada("1200");  //VALIDO
//        instance.setHorasaida("1530");    //VALIDO
//        instance.setIdcampusorigem(1);    //VALIDO
//        instance.setIdmotorista(1);   //VALIDO
//        instance.setIdveiculo(1);     //VALIDO
//        instance.setIdviagemfretada(3);   //VALIDO
//        instance.setNumeropassageiros(4); //VALIDO
//        instance.setResponsavel("Trago"); //VALIDO
//        boolean expResult = true;
//        boolean result = instance.alterarViagemFretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //  fail("The test case is a prototype.");
//    }
//
//    @Test
//    public void testAlterarViagemFretada4() {
//        //TESTA ALTERAR VIAGEM FRETADA  COM  DATA INDISPONIVEL
//        System.out.println("alterarViagemFretada");
//        ViagemFretada instance = new ViagemFretada();
//        instance.setCpfresponsavel("02732234028");
//        instance.setDatachegada("20140207");
//        instance.setDatasaida("20140201");
//        instance.setDestino("Bolivia");
//        instance.setHorachegada("1300");
//        instance.setHorasaida("1230");
//        instance.setIdcampusorigem(1);
//        instance.setIdmotorista(1);
//        instance.setIdveiculo(1);
//        instance.setIdviagemfretada(3);
//        instance.setNumeropassageiros(4);
//        instance.setResponsavel("Trago1");
//        boolean expResult = false;
//        boolean result = instance.alterarViagemFretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        //  fail("The test case is a prototype.");
//    }
    //    /**
//     * Test of getIdviagemfretada method, of class ViagemFretada.
//     */
    //    @Test
//    public void testGetIdviagemfretada() {
//        System.out.println("getIdviagemfretada");
//        ViagemFretada instance = new ViagemFretada();
//        int expResult = 0;
//        int result = instance.getIdviagemfretada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setIdviagemfretada method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetIdviagemfretada() {
//        System.out.println("setIdviagemfretada");
//        int idviagemfretada = 0;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setIdviagemfretada(idviagemfretada);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getDatachegada method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetDatachegada() {
//        System.out.println("getDatachegada");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getDatachegada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setDatachegada method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetDatachegada() {
//        System.out.println("setDatachegada");
//        String datachegada = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setDatachegada(datachegada);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getHorachegada method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetHorachegada() {
//        System.out.println("getHorachegada");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getHorachegada();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setHorachegada method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetHorachegada() {
//        System.out.println("setHorachegada");
//        String horachegada = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setHorachegada(horachegada);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getDatasaida method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetDatasaida() {
//        System.out.println("getDatasaida");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getDatasaida();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setDatasaida method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetDatasaida() {
//        System.out.println("setDatasaida");
//        String datasaida = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setDatasaida(datasaida);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getHorasaida method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetHorasaida() {
//        System.out.println("getHorasaida");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getHorasaida();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setHorasaida method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetHorasaida() {
//        System.out.println("setHorasaida");
//        String horasaida = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setHorasaida(horasaida);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getIdmotorista method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetIdmotorista() {
//        System.out.println("getIdmotorista");
//        ViagemFretada instance = new ViagemFretada();
//        int expResult = 0;
//        int result = instance.getIdmotorista();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setIdmotorista method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetIdmotorista() {
//        System.out.println("setIdmotorista");
//        int idmotorista = 0;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setIdmotorista(idmotorista);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getResponsavel method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetResponsavel() {
//        System.out.println("getResponsavel");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getResponsavel();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setResponsavel method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetResponsavel() {
//        System.out.println("setResponsavel");
//        String responsavel = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setResponsavel(responsavel);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getIdveiculo method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetIdveiculo() {
//        System.out.println("getIdveiculo");
//        ViagemFretada instance = new ViagemFretada();
//        int expResult = 0;
//        int result = instance.getIdveiculo();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of verificaVeiculo method, of class ViagemFretada.
//     */
//    @Test
//    public void testVerificaVeiculo() {
//        System.out.println("verificaVeiculo");
//        int idveiculo = 0;
//        ViagemFretada instance = new ViagemFretada();
//        boolean expResult = false;
//        boolean result = instance.verificaVeiculo(idveiculo);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of methodoVerificaDatas method, of class ViagemFretada.
//     */
//    @Test
//    public void testMethodoVerificaDatas() {
//        System.out.println("methodoVerificaDatas");
//        ViagemFretada instance = new ViagemFretada();
//        boolean expResult = false;
//        boolean result = instance.methodoVerificaDatas();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of verificaMotorista method, of class ViagemFretada.
//     */
//    @Test
//    public void testVerificaMotorista() {
//        System.out.println("verificaMotorista");
//        int idMotorista = 0;
//        ViagemFretada instance = new ViagemFretada();
//        boolean expResult = false;
//        boolean result = instance.verificaMotorista(idMotorista);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setIdveiculo method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetIdveiculo() {
//        System.out.println("setIdveiculo");
//        int idveiculo = 0;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setIdveiculo(idveiculo);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getIdcampusorigem method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetIdcampusorigem() {
//        System.out.println("getIdcampusorigem");
//        ViagemFretada instance = new ViagemFretada();
//        int expResult = 0;
//        int result = instance.getIdcampusorigem();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setIdcampusorigem method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetIdcampusorigem() {
//        System.out.println("setIdcampusorigem");
//        int idcampusorigem = 0;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setIdcampusorigem(idcampusorigem);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getDestino method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetDestino() {
//        System.out.println("getDestino");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getDestino();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setDestino method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetDestino() {
//        System.out.println("setDestino");
//        String destino = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setDestino(destino);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getCpfresponsavel method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetCpfresponsavel() {
//        System.out.println("getCpfresponsavel");
//        ViagemFretada instance = new ViagemFretada();
//        String expResult = "";
//        String result = instance.getCpfresponsavel();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setCpfresponsavel method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetCpfresponsavel() {
//        System.out.println("setCpfresponsavel");
//        String cpfresponsavel = "";
//        ViagemFretada instance = new ViagemFretada();
//        instance.setCpfresponsavel(cpfresponsavel);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getNumeropassageiros method, of class ViagemFretada.
//     */
//    @Test
//    public void testGetNumeropassageiros() {
//        System.out.println("getNumeropassageiros");
//        ViagemFretada instance = new ViagemFretada();
//        int expResult = 0;
//        int result = instance.getNumeropassageiros();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setNumeropassageiros method, of class ViagemFretada.
//     */
//    @Test
//    public void testSetNumeropassageiros() {
//        System.out.println("setNumeropassageiros");
//        int numeropassageiros = 0;
//        ViagemFretada instance = new ViagemFretada();
//        instance.setNumeropassageiros(numeropassageiros);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
