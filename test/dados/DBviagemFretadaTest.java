/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.ResultSet;
import java.util.ArrayList;
import model.ViagemFretada;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GuTo
 */
public class DBviagemFretadaTest {
    
    public DBviagemFretadaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insert method, of class DBviagemFretada.
     */
    @Test
    public void testInsert() {
        //TESTA INSERCAO DE VIAGEM FRETADA SEM PARAMETROS
        System.out.println("insert");
        String Datachegada = "";
        String Horachegada = "";
        String Horasaida = "";
        String Datasaida = "";
        int Idmotorista = 0;
        String Responsavel = "";
        int Idveiculo = 0;
        int Idcampusorigem = 0;
        String Destino = "";
        String Cpfresponsavel = "";
        int Numeropassageiros = 0;
        DBviagemFretada instance = new DBviagemFretada();
        instance.insert(Datachegada, Horachegada, Horasaida, Datasaida, Idmotorista, Responsavel, Idveiculo, Idcampusorigem, Destino, Cpfresponsavel, Numeropassageiros);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class DBviagemFretada.
     */
    @Test
    public void testUpdate() {
        //TESTA ALTERAÇÃO DE VIAGEM FRETADA
        System.out.println("update");
        String Datachegada = "";
        String Horachegada = "";
        String Horasaida = "";
        String Datasaida = "";
        int Idmotorista = 0;
        String Responsavel = "";
        int Idveiculo = 0;
        int Idcampusorigem = 0;
        String Destino = "";
        String Cpfresponsavel = "";
        int Numeropassageiros = 0;
        int Idviagemfretada = 0;
        DBviagemFretada instance = new DBviagemFretada();
        instance.update(Datachegada, Horachegada, Horasaida, Datasaida, Idmotorista, Responsavel, Idveiculo, Idcampusorigem, Destino, Cpfresponsavel, Numeropassageiros, Idviagemfretada);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of delete method, of class DBviagemFretada.
     */
    @Test
    public void testDelete() {
        //TESTA EXCLUSAO DE VIAGEM FRETADA 
        System.out.println("delete");
        int id = 0;
        DBviagemFretada instance = new DBviagemFretada();
        instance.delete(id);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of getResultSet method, of class DBviagemFretada.
     */
    @Test
    public void testGetResultSet() {
        //TESTA RETORNO DO "RESULTSET" DO SQL
        System.out.println("getResultSet");
        DBviagemFretada instance = new DBviagemFretada();
        ResultSet expResult = null;
        ResultSet result = instance.getResultSet();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

    /**
     * Test of closedConnection method, of class DBviagemFretada.
     */
    @Test
    public void testClosedConnection() {
        //TESTA LOGOF DO BANCO
        System.out.println("closedConnection");
        DBviagemFretada instance = new DBviagemFretada();
        instance.closedConnection();
        // TODO review the generated test code and remove the default call to fail.
     //   fail("The test case is a prototype.");
    }

//    /**
//     * Test of listar method, of class DBviagemFretada.
//     */
    @Test
    public void testListar() {
        /*TESTA SE ESTA LISTANDO VIAGENS CADASTRADAS
         */
        System.out.println("listar");
                ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
        
        DBviagemFretada instance = new DBviagemFretada();
        ArrayList expResult = fretada;
        
        ArrayList result = instance.listar(fretada);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      //  fail("The test case is a prototype.");
    }
}
