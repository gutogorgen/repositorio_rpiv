package dados;

import java.sql.*;

/**
 * 
 * @author Matheus
 * 
 */
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class acessoBanco {
    
    private static Statement stm;
    private static boolean aux = false;
    private static String JBDC_DRIVER = "org.postgresql.Driver";
	private static String DATABASE_URL = "jdbc:postgresql://localhost:5432/resolucao4";
	private static String usuario = "postgres";
	private static String senha = "postgres";
	private static Connection con;
    
    public acessoBanco (String dataBaseURL, String usuario, String senha){
        
       /* this.dataBaseURL = dataBaseURL;
        this.usuario = usuario;
        this.senha = senha;
        */
    	
        if (aux) {
        	
    	    System.out.println("Objeto já foi criado!");
            System.exit(0);
            
        }
        aux = true;

    }

    acessoBanco() {}

    public void setUsuario(String usuario) {
        
        this.usuario = usuario;
    }

    public String getUsuario() {
        
        return this.usuario;
    }

    public void setSenha(String senha) {
        
        this.senha = senha;
    }

    public String getSenha() {
        
        return this.senha;
    }

    /**
     * Realiza conexão com um banco de dados PostGres.
     * @return true se a conexão foi realizado com sucesso, ou false caso contrário.
     */
    public static boolean connect() {
        
        try{
        	
            Class.forName(JBDC_DRIVER).getInterfaces();
            
            con = DriverManager.getConnection(DATABASE_URL, usuario, senha);
            
            
            return true;
            
        }catch (SQLException sqle){
            
            System.err.println("Não foi possível a conexão com o Banco de Dados");
            sqle.printStackTrace();
            return false;
        }catch (ClassNotFoundException cnfe){
            
            cnfe.printStackTrace();
            return false;
        }
        
    }

    /**
     * Encerra a conexão com o banco de dados.
     * @return true se a desconexão foi concluí­da com sucesso, ou false caso contrário.
     */ 
    public static boolean disconnect() {
        
        try{
            
            con.close();
            return true;
        }catch (SQLException sqle){
            
            System.err.println("Não foi possível encerrar a conexão");
            sqle.printStackTrace();
            return false;
        }
    }

    /**
     * Conecta-se ao banco de dados e executa uma instrução SQL de consulta ao banco de dados.
     * Concluí­da com sucesso ou não a consulta, a conexão com o banco de dados encerrada.
     * @param sql - Instrução SQL de consulta ao banco de dados.
     * @return Um objeto ResultSet contendo os dados obtidos pela consulta.
     */
    public static ResultSet ListarBanco(String sql) {
        
        connect();
        try{
            
            stm = con.createStatement();
            ResultSet result = stm.executeQuery(sql);
            return result;
            
        }catch (SQLException sqle){
            
            System.err.println("SQL Inválido");
            sqle.printStackTrace();;
            return null;
        }
    }
    
    public static Connection conexaoRel() {
    	connect();
    	
    	return con;
    }

    /**
     * Conecta-se ao banco de dados e executa instruções SQL sobre o mesmo.
     * Permite executar as instruções insert, update e delete.
     * Tendo sida executada com sucesso a instrução ou não a conexão com o banco de dados é encerrada.
     * @param sql Instrução SQL a ser executada.
     * @return true se a instrução foi executada com sucesso ou false caso contrário.
     */
    public static  boolean AdicionarComandoNoBanco(String sql) {
        
        connect();
        try{
            
            stm = con.createStatement();
            stm.executeUpdate(sql);
            return true;
        } catch (SQLException sqle){
            
            System.err.println("SQL Inválida");
            sqle.printStackTrace();;
            return false;
        } finally{
            
            disconnect();
        }
    }

  
}
