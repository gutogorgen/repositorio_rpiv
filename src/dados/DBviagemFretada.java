/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.ViagemFretada;

/**
 *
 * @author GuTo
 */
public class DBviagemFretada {

    
    
    
    ConectaBD conectaDB;
    ResultSet resultSet = null;
    
    private static final String tabela = "viagemfretada";
    
    public DBviagemFretada(){
    
        conectaDB = new ConectaBD();
    }
   

//    public int totalResultados(ResultSet resultSetParametro) {
//   
//         int contador = 1;
//            
//         try{
//                while ( resultSetParametro.next() ){
//
//                    contador++;
//                }
//
//                resultSetParametro.beforeFirst();
//
//                return contador - 1;
//                
//        }catch(SQLException sqlException){
//               
//               JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());    
//	       //System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
//	}
//         return 0;
//             }
//   
    public boolean insert(String Datachegada,String Horachegada,String Horasaida,String Datasaida,
            int Idmotorista,String Responsavel,String placa,int Idcampusorigem,String Destino,
            String Cpfresponsavel,int Numeropassageiros) {
  String tabela = this.tabela;
		try{  
                        String query = "INSERT INTO "+tabela+"(datachegada,horachegada,datasaida,horasaida,idmotorista,responsavel,placa,idcampusorigem,destino,"
                                + "cpfresponsavel,numeropassageiros) VALUES "
                                + "('"+Datachegada+"','"+Horachegada+"','"+
              Datasaida+"','"+Horasaida+"','"+Idmotorista+"','"+Responsavel+"','"+placa+"','"+Idcampusorigem+"','"+Destino+"','"+Cpfresponsavel+"','"+Numeropassageiros+"')";
			
                        if( conectaDB.getStatement().executeUpdate(query) == 0 ){
				//System.err.println("Erro, ocorreu algum problema ao tentar inserir dados!");
                                JOptionPane.showMessageDialog(null, "Erro, ocorreu algum problema ao tentar inserir dados!");
                        }else{
                            closedConnection(); 
                            return true;
                                //JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!");
				//System.out.println("Dados do objeto inseridos com sucesso!");
                        }	
			
		}catch(SQLException sqlException){
                        JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());    
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
                closedConnection(); 
        return false;
    }

    public boolean update(String Datachegada,String Horachegada,String Horasaida,String Datasaida,
            int Idmotorista,String Responsavel,String placa,int Idcampusorigem,String Destino,
            String Cpfresponsavel,int Numeropassageiros,int Idviagemfretada) {

        
                String tabela = this.tabela;
                PreparedStatement pstmt = null;
		
		try{
                        
			String query = "UPDATE "+tabela+" SET (datachegada,horachegada,datasaida,horasaida,idmotorista,responsavel,placa,"
                                + "idcampusorigem,destino,cpfresponsavel,numeropassageiros) = ('"+Datachegada+"','"+Horachegada+"','"+Datasaida+"','"+Horasaida+"','"+Idmotorista+"',"
                                + "'"+Responsavel+"','"+placa+"','"+Idcampusorigem+"','"+Destino+"','"+Cpfresponsavel+"','"+Numeropassageiros+"') WHERE idviagemfretada = "+Idviagemfretada+"";
                        
                        if( conectaDB.getStatement().executeUpdate(query) == 0 ){
				        //	System.err.println("Alteração não foi realizada");  

				//System.err.println("Erro, ocorreu algum problema ao tentar atualizar dados!");
                                JOptionPane.showMessageDialog(null, "Erro, ocorreu algum problema ao tentar atualizar dados!");
                        }else{
                            closedConnection(); 
                            return true;
                                //JOptionPane.showMessageDialog(null, "Dados atualizados com sucesso!");
				//System.out.println("Dados do objeto atualizados com sucesso!");
                        }
		}catch(SQLException sqlException){
			
                        JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
                closedConnection(); 
        return false;
    
    }

    public boolean delete(int id) {

        
                String tabela = this.tabela;
            
		try{
			int retornoQuery = conectaDB.getStatement().executeUpdate("DELETE FROM "+tabela+" WHERE idviagemfretada = "+ id +"" );
			
	        if(retornoQuery == 0) {  
	        	//System.err.println("Exclusão não foi realizada");  
                        
                        JOptionPane.showMessageDialog(null, "Problema, Exclusão NÃO foi realizada", "Erro na exclusão de dados", JOptionPane.ERROR_MESSAGE);
	        } else {  
                    closedConnection(); 
                    return true;
                         

                    //JOptionPane.showMessageDialog(null, "Exclusão realizada com Sucesso!");
	            //System.out.println("Exclusão realizada com Sucesso");  
	        }  
			
		}catch(SQLException sqlException){
			
			System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
                closedConnection(); 
        return false;
    }

    public ResultSet getResultSet() {

        return this.resultSet;
    
    }

    public void closedConnection() {
     try{
		
                        conectaDB.getConnection().close();
			
//                        if( conectaDB.getConnection().isClosed() )
//			    System.out.println("Conexão fechada com sucesso");
//			else
//			    System.err.println("Ocorreram problemas ao tentar fechar a conexão");

		}catch(Exception exception){
                    
                    System.err.println("Ocorreram problemas ao tentar fechar a conexão, "+exception);
		}    
    }


    public ArrayList<ViagemFretada> listar(ArrayList<ViagemFretada> fretada) {
        
                     String tabela = this.tabela;
                try{
                        Statement stmt = conectaDB.getConnection().createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
                        
                        resultSet = stmt.executeQuery("SELECT * FROM "+tabela+"");
                        
                        try {
                while(getResultSet().next() ){
                       
                       
                       ViagemFretada viagFret = new ViagemFretada();
                      
                       
                       viagFret.setCpfresponsavel(getResultSet().getString("cpfresponsavel"));//nao tenho certeza
                       viagFret.setDatachegada(getResultSet().getString("datachegada"));
                       viagFret.setDatasaida(getResultSet().getString("datasaida") );//nao tenho certeza
                       viagFret.setDestino(getResultSet().getString("destino"));
                       viagFret.setHorachegada(getResultSet().getString("horachegada") );//nao tenho certeza
                       viagFret.setHorasaida(getResultSet().getString("horasaida"));
                       viagFret.setIdcampusorigem( Integer.parseInt(getResultSet().getString("idcampusorigem")));
                       viagFret.setIdmotorista( Integer.parseInt(getResultSet().getString("idmotorista")));
                       viagFret.setPlaca( getResultSet().getString("placa"));
                       viagFret.setIdviagemfretada( Integer.parseInt(getResultSet().getString("idviagemfretada")));
                       viagFret.setNumeropassageiros( Integer.parseInt(getResultSet().getString("numeropassageiros")));
                       viagFret.setResponsavel(getResultSet().getString("responsavel"));
                       //adiciona o item a lista de categorias do retorno
                      fretada.add( viagFret );
                      
                 }
            } catch (SQLException ex) {
              //  System.out.print("CATCH1");
                 Logger.getLogger(ViagemFretada.class.getName()).log(Level.SEVERE, null, ex);
                 return null;
            }
                }catch(SQLException sqlException){
                        
                        System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
                        return null;
                }
               return fretada;
        
    }
     public ResultSet select(int id) {

            
            String tabela = this.tabela;
                try{
                        


                    Statement stmt = conectaDB.getConnection().createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
                //Statement stmt = conectaDB.statement;

		//  = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

                
                resultSet = stmt.executeQuery("SELECT * FROM "+tabela+" WHERE idviagemfretada = "+id);

                //System.out.println("ooooo"+getResultSet().getString("idviagemfretada"));

                   // resultSet = conectaDB.getStatement().executeQuery("SELECT "+coluna+" FROM "+tabela+""+parametro);
  
                   // System.out.println("ta certo");
                        return resultSet;   
		}catch(SQLException sqlException){
			
                    System.out.println(sqlException);
                    return null;
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}

                
    
    }
        

}