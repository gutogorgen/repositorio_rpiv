create database rp4;
\c rp4;
create sequence seq_caronista;
create sequence seq_usuario;

CREATE TABLE usuario (

  idUsuario integer DEFAULT nextval('seq_usuario') NOT NULL,
  nome VARCHAR(45) not NULL,
  senha VARCHAR(45) not NULL,
  email VARCHAR(45) not NULL,
  cpf VARCHAR(11) not NULL,
  campusOrigem INT not NULL,
  nomeUsuario VARCHAR(45) not NULL,
  dataNascimento DATE not NULL,
  PRIMARY KEY(idUsuario));

CREATE TABLE caronista (
  idCaronista integer DEFAULT nextval('seq_caronista') NOT NULL,
  usuarioId INTEGER not null,
  matricula VARCHAR(45) not NULL,
  PRIMARY KEY(matricula),
  FOREIGN KEY(usuarioId) REFERENCES usuario(idUsuario));

CREATE TABLE Campus(
idCampus SERIAL PRIMARY KEY,
nome Varchar (25) NOT NULL);

CREATE TABLE Veiculo(
placa varchar(20) not null,
marca Varchar (20) not null,
modelo Varchar (20) not null,
capacidade int not null,
ano int not null,
quilometragem varchar(20) not null,
idCampusOrigem int not null,
cor varchar(20) not null,
status varchar(20) not null,
primary key (placa),
foreign key (idCampusOrigem) REFERENCES Campus(idCampus));

CREATE TABLE Motorista(
idMotorista SERIAL PRIMARY KEY,
cnh Varchar(11) NOT NULL,
dataContratacao varchar(8) NOT NULL,
idUsuario int NOT NULL,
idCampusOrigem int NOT NULL,
foreign key (idUsuario) REFERENCES Usuario(idUsuario),
foreign key (idCampusOrigem) REFERENCES Campus(idCampus));


CREATE TABLE ViagemFretada(
idViagemFretada SERIAL PRIMARY KEY,
datachegada Varchar(8) NOT NULL,
horaChegada Varchar(20) NOT NULL,
horaSaida Varchar(20) NOT NULL,
datasaida Varchar(8) NOT NULL,
idMotorista int NOT NULL,
responsavel Varchar(25) NOT NULL,
placa varchar(20) NOT NULL,
idCampusOrigem int NOT NULL,
destino Varchar(20) NOT NULL,
cpfResponsavel Varchar(11) NOT NULL,
numeroPassageiros int NOT NULL,
foreign key (idMotorista) REFERENCES Motorista(idMotorista),
foreign key (placa) REFERENCES Veiculo(placa),
foreign key (idCampusOrigem) REFERENCES Campus(idCampus));

CREATE TABLE ViagemNormal(
idViagem SERIAL PRIMARY KEY,
datasaida Varchar(20) NOT NULL,
horaSaida Varchar(20) NOT NULL,
idCampusOrigem int NOT NULL,
material Varchar(20),
idCampusDestino int NOT NULL,
idMotorista int NOT NULL,
placa varchar(20) NOT NULL,
foreign key (idCampusOrigem) REFERENCES Campus(idCampus),
foreign key (idCampusDestino) REFERENCES Campus(idCampus),
foreign key (idMotorista) REFERENCES Motorista(idMotorista),
foreign key (placa)REFERENCES Veiculo(placa));
