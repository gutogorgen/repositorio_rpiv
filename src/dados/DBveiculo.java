/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Veiculo;

/**
 *
 * @author GuTo
 */
public class DBveiculo {

    
    
    ConectaBD conectaDB ;
    ResultSet resultSet ;
    
    private static final String tabela = "veiculo";
    
    public DBveiculo(){
    
        conectaDB = new ConectaBD();
    }
   
    
    public ResultSet select(String coluna, String parametro) {

            
            String tabela = this.tabela;
                try{
                        


                    Statement stmt = conectaDB.getConnection().createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
                //Statement stmt = conectaDB.statement;

		//  = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);

                
                resultSet = stmt.executeQuery("SELECT "+coluna+" FROM "+tabela+" WHERE placa = "+"'"+parametro+"'");


                   // resultSet = conectaDB.getStatement().executeQuery("SELECT "+coluna+" FROM "+tabela+""+parametro);
  
                   // System.out.println("ta certo");
                        return resultSet;   
		}catch(SQLException sqlException){
			
                    System.out.println("deu pau" + sqlException);
                    return null;
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}

                
    
    }
        

  
    public int totalResultados(ResultSet resultSetParametro) {
   
         int contador = 1;
            
         try{
                while ( resultSetParametro.next() ){

                    contador++;
                }

                resultSetParametro.first();

                return contador - 1;
                
        }catch(SQLException sqlException){
               
               JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());    
	       //System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
	}
         return 0;
             }

    public void insert(String colunas, String valores) {
  String tabela = this.tabela;
		try{  
                        String query = "INSERT INTO "+tabela+"("+colunas+") VALUES ("+valores+")";
			
                        if( conectaDB.getStatement().executeUpdate(query) == 0 ){
				//System.err.println("Erro, ocorreu algum problema ao tentar inserir dados!");
                                JOptionPane.showMessageDialog(null, "Erro, ocorreu algum problema ao tentar inserir dados!");
                        }else{
                                //JOptionPane.showMessageDialog(null, "Dados inseridos com sucesso!");
				//System.out.println("Dados do objeto inseridos com sucesso!");
                        }	
			
		}catch(SQLException sqlException){
                        JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());    
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
    }

    public void update(String colunasValores, String placa) {

        
                String tabela = this.tabela;
                PreparedStatement pstmt = null;
		
		try{
                        
			String query = "UPDATE "+tabela+" SET "+colunasValores+" WHERE placa = "+"'"+placa+"'";
                        
                        if( conectaDB.getStatement().executeUpdate(query) == 0 ){
			
				//System.err.println("Erro, ocorreu algum problema ao tentar atualizar dados!");
                                JOptionPane.showMessageDialog(null, "Erro, ocorreu algum problema ao tentar atualizar dados!");
                        }else{
                                //JOptionPane.showMessageDialog(null, "Dados atualizados com sucesso!");
				//System.out.println("Dados do objeto atualizados com sucesso!");
                        }
		}catch(SQLException sqlException){
			
                        JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());
			//System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
    
    }

    public void delete(String id) {

        
                String tabela = this.tabela;
            
		try{
			int retornoQuery = conectaDB.getStatement().executeUpdate("DELETE FROM "+tabela+" WHERE placa = "+ id +"" );
			
	        if(retornoQuery == 0) {  
	        	System.err.println("Exclusão não foi realizada");  
                        
                        JOptionPane.showMessageDialog(null, "Problema, Exclusão NÃO foi realizada", "Erro na exclusão de dados", JOptionPane.ERROR_MESSAGE);
	        } else {  
                    
                    //JOptionPane.showMessageDialog(null, "Exclusão realizada com Sucesso!");
	            //System.out.println("Exclusão realizada com Sucesso");  
	        }  
			
		}catch(SQLException sqlException){
			
			System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
		}
    }

    
    public ResultSet getResultSet() {

        return this.resultSet;
    
    }

    public void closedConnection() {
     try{
		
                        conectaDB.getConnection().close();
			
                        if( conectaDB.getConnection().isClosed() )
			    System.out.println("Conexão fechada com sucesso");
			else
			    System.err.println("Ocorreram problemas ao tentar fechar a conexão");

		}catch(Exception exception){
                    
                    System.err.println("Ocorreram problemas ao tentar fechar a conexão, "+exception);
		}    
    }
    
        
}
