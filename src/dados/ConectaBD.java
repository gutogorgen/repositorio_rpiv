package dados;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.io.*;
import java.net.*;
import java.lang.*;
import javax.swing.JOptionPane;



public class ConectaBD {
	
      
    
       //CONFIGURAÇÕES SERVIDOR LOCAL
        private static final String bancoDados = "jdbc:postgresql://localhost:5432/rp4"; //COM porta (5432) especificada
        //private static final String bancoDados = "jdbc:postgrees://localhost/resolucao4"; //SEM porta especificada
        private static final String usuario = "postgres";
	private static final String senha = "postgres";
        final private String driver = "org.postgresql.Driver";      

/*    
        //CONFIGURAÇÕES SERVIDOR TECLA

        private static final String bancoDados = "jdbc:mysql://mysql04.bighost.com.br/mvfesta_patrimonio";
	private static final String usuario = "mvfesta_patadmin";
	private static final String senha = "patadmin1020";       
*/        
	private Connection con  = null;          // gerencia a conexão
	Statement statement = null;		// instrução de consulta
	ResultSet resultSet = null;	// gerencia resultados

            
	/**
     *
     */
    public ConectaBD(){

		try {
                        Class.forName(driver);
			con = DriverManager.getConnection(this.bancoDados, this.usuario, this.senha);
			statement  = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
      //  System.out.println("conectou");
               }catch(ClassNotFoundException erroDriver){        

                       JOptionPane.showMessageDialog(null, "Driver não localizado \nEXCEPTION: "+erroDriver, "ERRO", JOptionPane.ERROR_MESSAGE);   
		} catch (SQLException e) {

			//e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "EXCEPTION: "+e, "Erro", JOptionPane.ERROR_MESSAGE);   
		}
	}

        public Connection getConnection() {
            return con;
        }

        public void setConnection(Connection connection) {
            this.con = connection;
        }
        
        
        public Statement getStatement(){
            
            return statement;
        }
        
        
        public ResultSet getResultSet(){
        
            return this.resultSet;
        }
        

        

 
	
}
