package dados;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Usuario;

public class DBUsuario {
   
    acessoBanco banco = new acessoBanco();
    static ResultSet resultSet = null;
    
   public static ResultSet listar(String where) {
       
        String sql = null;
        
       if (where != null) {
        sql = "select * from usuario where "+where;
			
       } else {
        sql = "select * from usuario";
       }
       //System.out.println("sql listar usuario: "+sql);
      resultSet = acessoBanco.ListarBanco(sql);
      return resultSet;
    }
	
   public int totalResultados(ResultSet resultSetParametro){
            
         int contador = 1;
            
         try{
                while ( resultSetParametro.next() ){

                    contador++;
                }

                resultSetParametro.beforeFirst();

                return contador - 1;
                
        }catch(SQLException sqlException){
               
               JOptionPane.showMessageDialog(null, "Exceção SQL  "+ sqlException.getMessage());    
	       //System.out.printf("\nExceção SQL %s\n ", sqlException.getMessage());
	}
         return 0;
         
      }
    
    public boolean insert (String nomeCompleto,String nomeUser, String senha, String email, int campusOrigem,String dataNascimento, String cpf){
		
      String sql = "insert into usuario (nome,senha,nomeUsuario,email,cpf,campusOrigem,dataNascimento)VALUES "
                    + "('"+nomeCompleto+"','"+senha+"','"+nomeUser+"','"+email+"','"+cpf+"','"+campusOrigem+"','"+dataNascimento+"')";
      System.out.println("Sql insert usuario:"+sql);
      
      return banco.AdicionarComandoNoBanco(sql);                
                        
    }   
        

        
    
    public boolean update(String nomecompleto,String nomeUser, String  senha, String  email,int  campusdeorigem, String datadenascimento){
		
       String sql = "UPDATE usuario SET (nome,senha,email,campusOrigem,dataNascimento) = ('"+nomecompleto+"','"+senha+"','"+email+"','"+campusdeorigem+"','"+datadenascimento+"') WHERE  nomeusuario= '"+nomeUser+"'";
       System.out.println("Update usuario:"+sql);
       return banco.AdicionarComandoNoBanco(sql);
    }
 
    public void delete(String where){
         
        String sql = null;
        
       if (where != null) {
        sql = "delete from usuario where "+where;
	System.out.println("sql delete usuario:"+sql);
        acessoBanco.AdicionarComandoNoBanco(sql);		
       } 
       
		
    }
    
    
     public ResultSet getResultSet(){
            
            return this.resultSet;
     }

     
     
    public ResultSet verificaLogin(String senha, String login) {

        String sql = "select * from usuario where senha = '"+senha+"' and nomeusuario = '"+login+"'";
       // System.out.println("Sql verificar login: "+sql);
        return acessoBanco.ListarBanco(sql);
        
    }
    
    
    
    
}
