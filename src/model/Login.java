/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import dados.DBUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matheus
 */
public class Login {
    private String senha;
    private String login;
    private String nome;
    private String cpf;
    private String email;
    private int permisao;
    Usuario user = new Usuario();
    private ResultSet resultSet;
    Sessao sessao;
    
    public Usuario verificaAutorização(String senha,String login){
      
        Login log = new Login();
       return user = log.verificaLogin(senha, login);        
    }
    
    public Usuario verificaLogin(String Senha,String Login){
   
      sessao = new Sessao();
    
      
        DBUsuario dBUsuario = new DBUsuario();
        resultSet = dBUsuario.verificaLogin(Senha, Login);
                        try {

                             while(resultSet.next() ){

                               //user.setPermisao(Integer.parseInt(dBUsuario.getResultSet().getString("permisao")));
                               user.setCpf(resultSet.getString("cpf"));
                               user.setEmail(resultSet.getString("email"));
                               user.setIdusuario(Integer.parseInt(resultSet.getString("idusuario")));
                               user.setLogin(resultSet.getString("nomeusuario"));
                               user.setNome(resultSet.getString("nome"));
                               user.setSenha(resultSet.getString("senha"));
                               user.setCampusOrigem(Integer.parseInt(resultSet.getString("campusorigem")));
                              // System.out.println(" permissao : "+user.getPermisao());
                               /*
                               System.out.println("cpf : "+user.getCpf());
                               System.out.println("email : "+user.getEmail());
                               System.out.println("idusuario : "+user.getIdusuario());
                               System.out.println("login : "+user.getLogin());
                               System.out.println("nome : "+user.getNome());
                               System.out.println("senha : "+user.getSenha());
                               */
                               sessao.setUsuario(user);
                               
                             }
                       }catch (SQLException ex) {
                           System.out.println("erro1"+ex);
                           Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                           return null;
                       }     
                        return user;        
    
    }
    
/*public Usuario getUsuario(int idUsuario){
        DBUsuario dBUsuario = new DBUsuario();
        
            try{
                    //dBUsuario.select(getSenha(),getLogin());
                    dBUsuario.select(idUsuario);
            }catch(Exception ex){
                       System.out.println(ex);
                             return null;
                   }        
                    int totalResultado = dBUsuario.totalResultados(dBUsuario.getResultSet());
            
                   System.out.println("total resultado: "+ totalResultado);
                   if(totalResultado == 1){
                       Usuario user = new Usuario();
                        try {

                             while( dBUsuario.getResultSet().next() ){

                               user.setSenha(dBUsuario.getResultSet().getString("senha") );
                               user.setPermisao(Integer.parseInt(dBUsuario.getResultSet().getString("permisao")));
                               user.setNome(dBUsuario.getResultSet().getString("nome"));
                               user.setLogin(dBUsuario.getResultSet().getString("login"));
                               user.setIdusuario(Integer.parseInt(dBUsuario.getResultSet().getString("idusuario")));
                               user.setEmail( dBUsuario.getResultSet().getString("email"));
                               user.setCpf( dBUsuario.getResultSet().getString("cpf"));
                               System.out.println(user.getNome());                               

                               dBUsuario.closedConnection();
                             }
                       }catch (SQLException ex) {
                           System.out.println(ex);
                           Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                       }     
                        return user;
                   }else{
                   return null;                
                   }
         
    }*/

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the permisao
     */
    public int getPermisao() {
        return permisao;
    }

    /**
     * @param permisao the permisao to set
     */
    public void setPermisao(int permisao) {
        this.permisao = permisao;
    }
}
                   
