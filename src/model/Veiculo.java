/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBveiculo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GuTo
 */
public class Veiculo {
   private String placa; 
   private String marca;
   private String modelo;
   private int capacidade;
   private int ano;
   private String quilometragem;
   private int idcampusOrigem;
   private String cor;
   private String status;
   
   
   public Veiculo() {}

   public boolean inserirVeiculo(){
       
       DBveiculo dbVeiculo = new DBveiculo();
     
       dbVeiculo.insert("placa,marca,modelo,capacidade,ano,quilometragem,idcampusOrigem,cor,status", "'"+getPlaca()+"','"+getMarca().toString()+"','"+getModelo().toString()+"','"+
               getCapacidade()+"','"+getAno()+"','"+getQuilometragem().toString()+"','"+getIdcampusOrigem()+"','"+getCor()+"','"+getStatus()+"'");
       
       
       dbVeiculo.closedConnection();
       return true;
   }
   
   public boolean alterarVeiculo(){
       
       DBveiculo dbVeiculo = new DBveiculo();
       
       dbVeiculo.update("(marca,modelo,capacidade,ano,quilometragem,idcampusOrigem,cor,status) = ('"+getMarca().toString()+"','"+
               getModelo().toString()+"','"+getCapacidade()+"','"+getAno()+"','"+getQuilometragem().toString()+"','"+getIdcampusOrigem()+"','"+
               getCor()+"','"+getStatus()+"')",getPlaca());
       
       dbVeiculo.closedConnection();
       
       return true;
   }
   
   public boolean deletarVeiculo(Veiculo veiculo){
       
          DBveiculo dbVeiculo = new DBveiculo();

          dbVeiculo.delete(veiculo.getPlaca());
          
          dbVeiculo.closedConnection();
       
       return true;
   }
   
   public ArrayList<Veiculo> listarVeiculo(){
   
       DBveiculo dbVeiculo = new DBveiculo();
            
            ArrayList<Veiculo> veiculo = new ArrayList<Veiculo>();
            
            dbVeiculo.select("*","");
            
            try {
                while( dbVeiculo.getResultSet().next() ){
                       
                       
                       Veiculo veic = new Veiculo();
                      
                       
                       veic.setAno( Integer.parseInt(dbVeiculo.getResultSet().getString("ano")));//nao tenho certeza
                       veic.setCapacidade( Integer.parseInt(dbVeiculo.getResultSet().getString("capacidade")));
                       veic.setIdcampusOrigem( Integer.parseInt(dbVeiculo.getResultSet().getString("idcampusorigem")) );//nao tenho certeza
                       veic.setMarca( dbVeiculo.getResultSet().getString("marca"));
                       veic.setModelo(dbVeiculo.getResultSet().getString("modelo") );//nao tenho certeza
                       veic.setQuilometragem( dbVeiculo.getResultSet().getString("quilometragem"));
                       veic.setPlaca( dbVeiculo.getResultSet().getString("placa"));
                       
                       //adiciona o item a lista de categorias do retorno
                      veiculo.add( veic );
                       //System.out.println(veic.idveiculo +"--"+ veic.marca +"--"+veic.modelo+"--"+veic.quilometragem+""
                       //        +veic.capacidade+"--"+veic.idcampusOrigem+"--"+veic.ano);
                 }
                dbVeiculo.closedConnection();
            } catch (SQLException ex) {
                System.out.print(ex);
            }
       
       return null;
       
   }
    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the capacidade
     */
    public int getCapacidade() {
        return capacidade;
    }

    /**
     * @param capacidade the capacidade to set
     */
    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the quilometragem
     */
    public String getQuilometragem() {
        return quilometragem;
    }

    /**
     * @param quilometragem the quilometragem to set
     */
    public void setQuilometragem(String quilometragem) {
        this.quilometragem = quilometragem;
    }

    /**
     * @return the idcampusOrigem
     */
    public int getIdcampusOrigem() {
        return idcampusOrigem;
    }

    /**
     * @param idcampusOrigem the idcampusOrigem to set
     */
    public void setIdcampusOrigem(int idcampusOrigem) {
        this.idcampusOrigem = idcampusOrigem;
    }

 
        public Veiculo getVeiculo(String placa) {

                   DBveiculo dBveiculo = new DBveiculo();

                   try{
                    dBveiculo.select("*",placa);

                   }catch(Exception ex){
                       System.out.println(ex);
                             return null;
                   }

                   int totalResultado = dBveiculo.totalResultados(dBveiculo.getResultSet());

                   //System.out.println("total resultado: "+ totalResultado);
                   if(totalResultado == 1){
                       Veiculo veiculo = new Veiculo();
                        try {


                               veiculo.setAno( Integer.parseInt( dBveiculo.getResultSet().getString("ano") ) );
                               veiculo.setCapacidade(Integer.parseInt(dBveiculo.getResultSet().getString("capacidade")));
                               veiculo.setIdcampusOrigem(Integer.parseInt(dBveiculo.getResultSet().getString("idcampusorigem")) );
                               veiculo.setPlaca(dBveiculo.getResultSet().getString("placa") );
                               veiculo.setMarca(dBveiculo.getResultSet().getString("marca") );
                               veiculo.setModelo( dBveiculo.getResultSet().getString("modelo") );
                               veiculo.setQuilometragem( dBveiculo.getResultSet().getString("quilometragem") );
                               veiculo.setCor( dBveiculo.getResultSet().getString("cor") );
                               veiculo.setStatus( dBveiculo.getResultSet().getString("status") );
                               

                       }catch (SQLException ex) {
                          // System.out.println(ex);
                           Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                       }     
                        return veiculo;
                   }else{
                   return null;                
                   }

                   }           

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

            }  

