/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author matheus
 */
public class Administrador extends Usuario{
    private Usuario usuario;
	 
	private ViagemNormal viagem;
	 
	private Motorista motorista;
	 
	private Veiculo veiculo;
	 
	private Campus campus;
	 
	public boolean cancelarViagem(ViagemNormal normal) {
		return false;
	}
	 
	private boolean buscarCaronista(String cpf) {
		return false;
	}
	 
	public boolean cadastrarVeiculo(String placa, String marca, String modelo, String ano, int capacidade, String cor, String kilometragem, int campusOrigem) {
		return false;
	}
	 
	public boolean cadastratMotorista() {
		return false;
	}
	 
	public boolean cadastrarViagemFretada(Veiculo veiculo, Motorista motorista, Campus campusOrigem, String dataSaida, String horaSaida, String nomeResponsavel, int numResponsavel, String cpfResponsavel, String destino, String dataChegada, String horaChegada) {
		return false;
	}
	 
	public boolean cadastrarViagemNormal(Veiculo veiculo, Motorista motorista, Campus campusOrigem, Campus campusDestino, String MaterialTrnsportado, String dataSaida, String horaSaida) {
		return false;
	}
	 
	public boolean alterarVeiculo(String modelo, String ano, String marca, int capacidade, double quilometragem, Campus campus, String cor) {
		return false;
	}
	 
	public Veiculo buscarVeiculos(String placa) {
		return null;
	}
	 
	public ArrayList listarVeiculos(int campusOrigem) {
		return null;
	}
	 
	public boolean mudarStatusVeiculo(Veiculo veiculo) {
		return false;
	}
	 
	public Motorista buscarMotorista(String cpf) {
		return null;
	}
	 
	public ArrayList listarMotorista(Campus campusOrigem) {
		return null;
	}
	 
	public boolean desativarMotorista(Motorista motorista) {
		return false;
	}
	 
	public boolean alterarMotorista(String nome, String cnh, String tipoCNH, String email, Date datNasc, Date dateContrato, Date dataVencCNH) {
		return false;
	}
	 
	public boolean desativarVeiculo(Veiculo veiculo) {
		return false;
	}
	 
	public ViagemNormal buscaViagem(Veiculo veiculo, String dataPartida, String horaPartida) {
		return null;
	}
	 
	private boolean incluirCaronista(String nomeCompleto, 
                String login, 
                String senha, 
                String email, 
                String CPF, 
                String matricula, 
                String campusOrigem, 
                String dataNascimento) {
		return false;
	}
	 
	public boolean cancelarViagemFretada(ViagemFretada fretada) {
		return false;
	}
	 
	public boolean alterarViagemFretada(Veiculo veiculo, Motorista motorista, Campus campusOrigem, String dataSaida, String horaSaida, String nomeResponsavel, int numResponsavel, String cpfResponsavel, String destino, String dataChegada, String horaChegada) {
		return false;
	}
	 
	public ArrayList listarViagemFretada() {
		return null;
	}
	 
	public boolean transferirCaronista() {
		return false;
	}
	 
	public boolean verificaAutorizacao() {
		return false;
	}
	 
	public void gerarRelatorioViagens(Campus campusOrigem, String dataSaida) {
	 
	}
	 
	public List listarCaronista(String campusOrigem) {
		return null;
	}

}
