/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBviagemFretada;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GuTo
 */
public class ViagemFretada {

    private int idviagemfretada;
    private String datachegada;
    private String horachegada;
    private String datasaida;
    private String horasaida;
    private int idmotorista;
    private String responsavel;
    //private int idveiculo;
    private String placa;
    private int idcampusorigem;
    private String destino;
    private String cpfresponsavel;
    private int numeropassageiros;

    public ViagemFretada() {
    }

    public boolean inserirViagemFretada() {
        DBviagemFretada dbfretada = new DBviagemFretada();

        if (verificaViagemFretada() == true) {

            if (dbfretada.insert(getDatachegada(), getHorachegada(), getHorasaida(), getDatasaida(), getIdmotorista(), getResponsavel(),
                    getPlaca(), getIdcampusorigem(), getDestino(), getCpfresponsavel(), getNumeropassageiros()) == true) {
                return true;


            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean mostrarViagemFretada() {
        DBviagemFretada dbfretada = new DBviagemFretada();


        try {

            ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();


            dbfretada.listar(fretada);

          //  System.out.println(fretada.size());
            for (ViagemFretada fret : fretada) {

                System.out.print(" RESPONSAVEL: " + fret.getResponsavel());
                System.out.print(" CPF: " + fret.getCpfresponsavel());
                System.out.print(" DATACHEGADA: " + fret.getDatachegada());
                System.out.print(" DATASAIDA: " + fret.getDatasaida());
                System.out.print(" DESTINO: " + fret.getDestino());
                System.out.print(" HORACHEGADA: " + fret.getHorachegada());
                System.out.print(" HORASAIDA: " + fret.getHorasaida());
                System.out.print(" IDCAMPUSORIGEM: " + fret.getIdcampusorigem());
                System.out.print(" IDMOTORISTA: " + fret.getIdmotorista());
                System.out.print(" PLACA: " + fret.getPlaca());
                System.out.print(" IDVOAGEMFRETADA: " + fret.getIdviagemfretada());
                System.out.print("\n");
            }
            return true;

        } catch (Exception ex) {
            System.out.print("NÃO FOI POSSIVEL EXECUTAR A LISTAGEM " + ex);

        }

        dbfretada.closedConnection();
        return false;
    }

    public boolean deletarViagemFretada(ViagemFretada viagemfretada) {
        DBviagemFretada dbfretada = new DBviagemFretada();

        if (dbfretada.delete(viagemfretada.getIdviagemfretada()) == true) {

            return true;
        } else {
            return false;
        }
    }

    public boolean alterarViagemFretada() {
        DBviagemFretada dbfretada = new DBviagemFretada();
        boolean retorno;
        if (verificaViagemFretada() == true) {

            if (dbfretada.update(getDatachegada(), getHorachegada(), getHorasaida(), getDatasaida(),
                    getIdmotorista(), getResponsavel(), getPlaca(), getIdcampusorigem(),
                    getDestino(), getCpfresponsavel(), getNumeropassageiros(), getIdviagemfretada()) == true) {

                return true;

            }
        }
        return false;

    }

    /**
     * @return the idviagemfretada
     */
    public int getIdviagemfretada() {
        return idviagemfretada;
    }

    /**
     * @param idviagemfretada the idviagemfretada to set
     */
    public void setIdviagemfretada(int idviagemfretada) {

        this.idviagemfretada = idviagemfretada;

    }

    /**
     * @return the datachegada
     */
    public String getDatachegada() {
        return datachegada;
    }

    /**
     * @param datachegada the datachegada to set
     */
    public void setDatachegada(String datachegada) {
        try {
            Integer.parseInt(datachegada);
            int i = datachegada.length();
            if (i == 8) {
                this.datachegada = datachegada;
            } else {
                this.datachegada = datachegada = "";
           //     System.out.println("DATA DEVE CONTER 8 NUMEROS");
            }

        } catch (Exception x) {
            System.out.println("FORMATO DE DATA_CHEGADA INVALIDO" + x);
        }

    }

    /**
     * @return the horachegada
     */
    public String getHorachegada() {
        return horachegada;
    }

    /**
     * @param horachegada the horachegada to set
     */
    public void setHorachegada(String horachegada) {
        try {
            Integer.parseInt(horachegada);
            int i = horachegada.length();
            if (i == 4) {
                this.horachegada = horachegada;
            } else {
                this.horachegada = horachegada = "";
              //  System.out.println("HORA DEVE CONTER 4 NUMEROS");
            }

        } catch (Exception x) {
            System.out.println("FORMATO DE HORA_CHEGADA INVALIDO" + x);
        }

    }

    /**
     * @return the datasaida
     */
    public String getDatasaida() {
        return datasaida;
    }

    /**
     * @param datasaida the datasaida to set
     */
    public void setDatasaida(String datasaida) {
        try {
            Integer.parseInt(datasaida);
            int i = datasaida.length();
            if (i == 8) {
                this.datasaida = datasaida;
            } else {
                this.datasaida = datasaida = "";
            //    System.out.println("DATA DEVE CONTER 8 NUMEROS");
            }

        } catch (Exception x) {
            System.out.println("FORMATO DE DATA_SAIDA INVALIDO" + x);
        }
    }

    /**
     * @return the horasaida
     */
    public String getHorasaida() {
        return horasaida;
    }

    /**
     * @param horasaida the horasaida to set
     */
    public void setHorasaida(String horasaida) {
        try {
            Integer.parseInt(horasaida);
            int i = horasaida.length();
            if (i == 4) {

                this.horasaida = horasaida;
            } else {
                this.horasaida = horasaida = "";
             //   System.out.println("DATA DEVE CONTER 4 NUMEROS");

            }
        } catch (Exception x) {
            System.out.println("FORMATO DE HORA_SAIDA INVALIDO" + x);
        }
    }

    /**
     * @return the idmotorista
     */
    public int getIdmotorista() {
        return idmotorista;
    }

    /**
     * @param idmotorista the idmotorista to set
     */
    public void setIdmotorista(int idmotorista) {

        this.idmotorista = idmotorista;

    }

    /**
     * @return the responsavel
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(String responsavel) {

        this.responsavel = responsavel;

    }

    public boolean verificaVeiculo(String placa) {

        Veiculo veiculo = new Veiculo();
        //System.out.println(" ID VEICULO  "+veiculo.getVeiculo(idveiculo).getModelo());
        //System.out.println(" LISTA VEICULO  "+veiculo.getVeiculo(idveiculo).equals(this.getIdveiculo()));

        if (veiculo.getVeiculo(placa) == null) {
          //  System.out.println("Veiculo ainda não cadastrado no sistema");
            return false;
        }
        if (methodoVerificaDatas() == true) {
            //ViagemFretada viagem = new ViagemFretada();
            if (veiculo.getVeiculo(placa).getIdcampusOrigem() == getIdcampusorigem()) {
                return true;

            } else {
               // System.out.println("Veiculo não pertence a este campus");
                return false;

            }
        } else {
          //  System.out.println("Veiculo não estara disponivel para essa data");
        }

        return false;


    }

    public boolean methodoVerificaDatas() {
        DBviagemFretada viagem = new DBviagemFretada();
        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();

        try {
            viagem.listar(fretada);

            //System.out.println(dbfretada.listar(fretada).get(idveiculo -1).getDestino());
            viagem.closedConnection();
        } catch (Exception ex) {
            System.out.print("NÃO FOI POSSIVEL EXECUTAR A LISTAGEM " + ex);

        }
      //  System.out.println("tamanho  " + fretada.size());
        if (fretada.size() == 0) {
            return true;
        }
        boolean retorno = false;
        for (ViagemFretada fret : fretada) {
            //   System.out.println(idveiculo);   
            //DateFormat format = new SimpleDateFormat("yyyyMMdd");  
            DateFormat formathora = new SimpleDateFormat("yyyyMMddKKmm");

            try {


                //saidaViagemCadastrada = new java.sql.Date(format.parse(fret.getDatasaida()).getTime());  
                //chegadaViagemCadastrada = new java.sql.Date(format.parse(fret.getDatachegada()).getTime());
                //chegadaNovaViagem = new java.sql.Date(format.parse(getDatachegada()).getTime());
                //saidaNovaViagem = new java.sql.Date(format.parse(getDatasaida()).getTime());

                String cNV = (getDatachegada() + getHorachegada());
                String sNV = (getDatasaida() + getHorasaida());
                String sVC = (fret.getDatasaida() + fret.getHorasaida());
                String cVC = (fret.getDatachegada() + fret.getHorachegada());

                java.util.Date chegadaNovaViagem = formathora.parse(cNV);
                java.util.Date saidaNovaViagem = formathora.parse(sNV);
                java.util.Date saidaViagemCadastrada = formathora.parse(sVC);
                java.util.Date chegadaViagemCadastrada = formathora.parse(cVC);

//          System.out.println("chegadaNovaViagem:   "+chegadaNovaViagem);
//          System.out.println("saidaNovaViagem:   "+saidaNovaViagem);
//          System.out.println("saidaViagemCadastrada:   "+saidaViagemCadastrada);
//          System.out.println("chegadaViagemCadastrada:   "+chegadaViagemCadastrada);



//                if(SaidaNovaViagem > chegadaViagemCadastrada && chegadaNovaViagem > SaidaNovaViagem || SaidaNovaViagem < saidaViagemCadastrada
//                           && chegadaNovaViagem > SaidaNovaViagem && chegadaNovaViagem < saidaViagemCadastrada){
                if (saidaNovaViagem.after(chegadaViagemCadastrada) == true && chegadaNovaViagem.after(saidaNovaViagem) == true || saidaNovaViagem.before(saidaViagemCadastrada)
                        && chegadaNovaViagem.after(saidaNovaViagem) == true && chegadaNovaViagem.before(saidaViagemCadastrada) == true) {
                    //           System.out.println("---OOOK---");
                    retorno = true;
                } else {
                    //  System.out.println("não estara disponivel nessa data");
                    retorno = false;
                }
            } catch (ParseException ex) {
                Logger.getLogger(ViagemFretada.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (retorno == true) {

            return true;

        }

        return false;
    }

    public boolean verificaMotorista(int idMotorista) {
        Motorista motorista = new Motorista();

        if (motorista.getMotorista(idMotorista) == null) {
          //  System.out.println("Motorista ainda não cadastrado no sistema");
            return false;
        }
        if (methodoVerificaDatas() == true) {

            return true;
        } else {
          //  System.out.println("Motorista não estara disponivel para essa data");
        }

        return false;


    }


    /**
     * @return the idcampusorigem
     */
    public int getIdcampusorigem() {
        return idcampusorigem;
    }

    /**
     * @param idcampusorigem the idcampusorigem to set
     */
    public void setIdcampusorigem(int idcampusorigem) {

        this.idcampusorigem = idcampusorigem;

    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the cpfresponsavel
     */
    public String getCpfresponsavel() {
        return cpfresponsavel;
    }

    /**
     * @param cpfresponsavel the cpfresponsavel to set
     */
    public void setCpfresponsavel(String cpfresponsavel) {
        this.cpfresponsavel = cpfresponsavel;
    }

    /**
     * @return the numeropassageiros
     */
    public int getNumeropassageiros() {
        return numeropassageiros;
    }

    /**
     * @param numeropassageiros the numeropassageiros to set
     */
    public void setNumeropassageiros(int numeropassageiros) {
        this.numeropassageiros = numeropassageiros;

    }

    //veiculo disponivel
    //motorista disponivel
    //campusOrigem valido
    //verificaDatas
    //verificaCPF
    //verificaCapacidade
    //verificaAutorizacao
    private boolean verificaCPF() {
        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();

        // fretada = listarViagemFretada();

        ValidaCPF validaCPF = new ValidaCPF();

        if (validaCPF.isCPF(getCpfresponsavel()) == true) {
           // System.out.print("CPF VALIDO");
            return true;
        } else if (validaCPF.isCPF(getCpfresponsavel()) == false) {
         //   System.out.print("CPF FALSO");
            return false;

        }
        for (ViagemFretada fret : fretada) {
//            System.out.println("cpf1 " + fret.cpfresponsavel);
//            System.out.println("cpf2 " + getCpfresponsavel());

            if (fret.cpfresponsavel.equals(getCpfresponsavel())) {

           //     System.out.println("AVISO" + "já existe uma viajem com o mesmo responsavel");
            }

        }

        return false;
    }

    private boolean verificaDatas() {


        DateFormat formathora = new SimpleDateFormat("yyyyMMddKKmm");

        try {
            String cNV = (getDatachegada() + getHorachegada());
            String sNV = (getDatasaida() + getHorasaida());

            java.util.Date chegadaNovaViagem = formathora.parse(cNV);
            java.util.Date saidaNovaViagem = formathora.parse(sNV);

            //System.out.print(data);

            if (getDatachegada() == null || getDatachegada().equals("")
                    && getDatasaida() == null || getDatasaida().equals("")) {
                return false;
            }
            if (chegadaNovaViagem.before(saidaNovaViagem)) {
           //     System.out.println("data de chegada anterior a de saida");
                return false;
            } else if (chegadaNovaViagem.equals(saidaNovaViagem)) {
                //if(ComparaHora() == false){
             //   System.out.println("DIA E HORA DE PARTIDA E DE RETORNO SÃO IGUAIS");

                return false;

            }
         //   System.out.println("o dia é hoje");
//                 }  

            //System.out.println(getDatasaida());
        } catch (ParseException ex) {
            Logger.getLogger(ViagemFretada.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
//    }else{
//                  return true;

    }

    private boolean verificaCapacidade(String placa) {
        Veiculo veiculo = new Veiculo();

        if (veiculo.getVeiculo(placa).getCapacidade() < getNumeropassageiros() + 1) {
            //System.out.println("numero de passageiros exede capacidade do veiculo");

            //System.out.println("numero de passageiros  " + getNumeropassageiros() + " MAIS O LUGAR DO MOTORISTA  supera a  capacidade  que é de " + veiculo.getVeiculo(idveiculo).getCapacidade());

            return false;
        }

        return true;
    }

    private boolean verificaViagemFretada() {
        if ((getCpfresponsavel() == "" || getCpfresponsavel() == null)
                || (getDatachegada() == "" || getDatachegada() == null)
                || (getDatasaida() == "" || getDatasaida() == null)
                || (getDestino() == "" || getDestino() == null)
                || (getHorachegada() == "" || getHorachegada() == null)
                || (getHorasaida() == "" || getHorasaida() == null)
                || (getIdcampusorigem() == 0)
                || (getIdmotorista() == 0)
                || (getPlaca() == "" || getPlaca() == null)
                || (getNumeropassageiros() == 0)
                || (getResponsavel() == "" || getResponsavel() == null)) {

         //   System.out.println("falta dados para insersao");

            return false;
        } else {
            Campus camp = new Campus();
            Veiculo veic = new Veiculo();
            Motorista motora = new Motorista();
//            try {
//                if (verificaAutorização() == true && camp.verificaCampus(getIdcampusorigem()) == true && verificaidVeiculo(getIdveiculo()) == true && verificaDatas() == true 
//                        && verificaCPF() == true && motora.verificaMotorista(idmotorista) == true){
            if (camp.verificaCampus(getIdcampusorigem()) == true && verificaDatas() == true
                    && camp.verificaCampus(idcampusorigem) == true
                    && verificaCPF() == true && verificaVeiculo(placa) == true
                    && verificaMotorista(idmotorista) == true && verificaCapacidade(placa)) {

                //  System.out.println(" TUDO OK"); 
                return true;
            } else {
                // System.out.println(" NÃO ROLOU AMIZADE");
            }

        }
        return false;
    }

    public ViagemFretada getViagemFretada(int id) {


        DBviagemFretada dBviagemFretada = new DBviagemFretada();

        try {
            dBviagemFretada.select(id);

        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }

        // int totalResultado = dBviagemFretada.totalResultados(dBviagemFretada.getResultSet());

        //System.out.println("total resultado: "+ totalResultado);
        // if(totalResultado == 1){
        ViagemFretada viagem = new ViagemFretada();
        try {


            while (dBviagemFretada.getResultSet().next()) {
                viagem.setIdviagemfretada(Integer.parseInt(dBviagemFretada.getResultSet().getString("idviagemfretada")));

            }

//                               viagem.setAno( Integer.parseInt( dBveiculo.getResultSet().getString("ano") ) );
//                               viagem.setCapacidade(Integer.parseInt(dBveiculo.getResultSet().getString("capacidade")));
//                               viagem.setIdcampusOrigem(Integer.parseInt(dBveiculo.getResultSet().getString("idcampusorigem")) );
//                               viagem.setIdveiculo(Integer.parseInt(dBveiculo.getResultSet().getString("idveiculo")) );
//                               viagem.setMarca(dBveiculo.getResultSet().getString("marca") );
//                               viagem.setModelo( dBveiculo.getResultSet().getString("modelo") );
//                               viagem.setQuilometragem( dBveiculo.getResultSet().getString("quilometragem") );
//                               

        } catch (SQLException ex) {
            System.out.println(ex);
            Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return viagem;
//                   }else{
//                   return null;                
//                   }

    }
    public void deletarTodasViagensFretadas(){
       //CRIADO PARA MELHORAR A LOGICA DOS TESTES!!!!
        ArrayList<ViagemFretada> fretada = new ArrayList<ViagemFretada>();
        DBviagemFretada dbfretada = new DBviagemFretada();
        dbfretada.listar(fretada);
        for(int i = 0; i < fretada.size(); i++){

           int id = fretada.get(i).getIdviagemfretada();
            dbfretada.delete(id);
            
        }
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
