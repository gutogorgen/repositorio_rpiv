/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBCampus;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GuTo
 */
public class Campus {
    
    private int idcampus;
    private String nome;

    public Campus() {}

     
    public boolean alterarCampus(Campus campus) {
	DBCampus dBcampus = new DBCampus();

            dBcampus.update(campus.getNome(), campus.getIdcampus());
            
                    
            dBcampus.closedConnection();
            return true;
	}
        public boolean inserirCampus() {
	
            DBCampus dBCampus = new DBCampus();
                       
           dBCampus.insert("nome",""+getNome().toString()+"");       
            
            dBCampus.closedConnection();
            return true;
	}
	
    public boolean deletarCampus(Campus campus) {
            
         
            DBCampus dbCampus = new DBCampus();
            dbCampus.delete(campus.getIdcampus());
            
            dbCampus.closedConnection();
            return true;
	}
    
    
     public ArrayList<Campus> listarCampus() {
		
            DBCampus dbCampus = new DBCampus();
            
            ArrayList<Campus> campus = new ArrayList<Campus>();
            
            dbCampus.select("*", " order by idcampus asc");
            
            try {
                while( dbCampus.getResultSet().next() ){
                       
                       
                       Campus camp = new Campus();
                      
                       
                       camp.setNome(dbCampus.getResultSet().getString("nome") );//nao tenho certeza
                       camp.setIdcampus( Integer.parseInt(dbCampus.getResultSet().getString("idCampus")));

                       //adiciona o item a lista de categorias do retorno
                       campus.add( camp );
                     //  System.out.println(camp.idcampus +"-----"+ camp.nome);
                 }
                dbCampus.closedConnection();
            } catch (SQLException ex) {
                System.out.print(ex);
                // Logger.getLogger(Item.class.getName()).log(Level.SEVERE, null, ex);
            }
                 
            return campus;
            
            
    }

    /**
     * @return the idcampus
     */
    public int getIdcampus() {
        return idcampus;
    }

    /**
     * @param idcampus the idcampus to set
     */
    public void setIdcampus(int idcampus) {
        this.idcampus = idcampus;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean verificaCampus(int idcampus){
        if(getCampus(idcampus) == null){
           // System.out.println("Campus não existe");
            return false;
        }
        
        return true;
    }
        public Campus getCampus(int id) {

                   DBCampus dBCampus = new DBCampus();

                   try{
                    dBCampus.select("*", " WHERE idcampus = "+id);

                       
                   }catch(Exception ex){
                       System.out.println(ex);
                             return null;
                   }

                   int totalResultado = dBCampus.totalResultados(dBCampus.getResultSet());

                   //System.out.println("total resultado: "+ totalResultado);
                   if(totalResultado == 1){
                       Campus camp = new Campus();
                       
                        try {
                            while( dBCampus.getResultSet().next() ){


                               camp.setIdcampus( Integer.parseInt( dBCampus.getResultSet().getString("idcampus") ) );
                               camp.setNome(dBCampus.getResultSet().getString("nome"));
                             //  System.out.println(camp.getNome());
                            }  

                       }catch (SQLException ex) {
                           System.out.println(ex);
                           Logger.getLogger(Campus.class.getName()).log(Level.SEVERE, null, ex);
                       }     
                        return camp;
                   }else{
                   return null;                
                   }

                   }           


}
