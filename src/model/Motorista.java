/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBmotorista;
import dados.DBviagemFretada;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GuTo
 */
public class Motorista {
    
    private int idmotorista;
    private String cnh;
    private String datacontratacao;
    private int idusuario;
    private int idcampusorigem;

    public Motorista() {}
    
   public boolean inserirMotorista(){
              DBmotorista dbmotorista = new DBmotorista();
       
              dbmotorista.insert("cnh,datacontratacao,idusuario,idcampusorigem", "'"+getCnh()+"','"+getDatacontratacao()+"','"+
                      getIdusuario()+"','"+getIdcampusorigem()+"'");
              
              
        //tenque tratar o retorno
       
       dbmotorista.closedConnection();
       return true;
    }
   public boolean alterarMotorista(){
              DBmotorista dbmotorista = new DBmotorista();

      
              dbmotorista.closedConnection();
       return true;
    }
   public boolean deletarMotorista(){
              DBmotorista dbmotorista = new DBmotorista();

              
       dbmotorista.closedConnection();
       return true;
    }
   public boolean listarMotorista(){
              DBmotorista dbmotorista = new DBmotorista();

              
       dbmotorista.closedConnection();       
       return true;
    }

    /**
     * @return the idmotorista
     */
    public int getIdmotorista() {
        return idmotorista;
    }

    /**
     * @param idmotorista the idmotorista to set
     */
    public void setIdmotorista(int idmotorista) {
        this.idmotorista = idmotorista;
    }

    /**
     * @return the cnh
     */
    public String getCnh() {
        return cnh;
    }

    /**
     * @param cnh the cnh to set
     */
    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    

    /**
     * @return the idusuario
     */
    public int getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the idcampusorigem
     */
    public int getIdcampusorigem() {
        return idcampusorigem;
    }

    /**
     * @param idcampusorigem the idcampusorigem to set
     */
    public void setIdcampusorigem(int idcampusorigem) {
        this.idcampusorigem = idcampusorigem;
    }

    /**
     * @return the datacontratacao
     */
    public String getDatacontratacao() {
        return datacontratacao;
    }

    /**
     * @param datacontratacao the datacontratacao to set
     */
    public void setDatacontratacao(String datacontratacao) {
        this.datacontratacao = datacontratacao;
    }
   
    
        public Motorista getMotorista(int id) {

                   DBmotorista dBmotorista = new DBmotorista();

                   try{
                    dBmotorista.select("*", " WHERE idmotorista = "+id);

                   }catch(Exception ex){
                       System.out.println(ex);
                             return null;
                   }

                   int totalResultado = dBmotorista.totalResultados(dBmotorista.getResultSet());

                   //System.out.println("total resultado: "+ totalResultado);
                   if(totalResultado == 1){
                       
                       Motorista motorista = new Motorista();
                        try {

                            while( dBmotorista.getResultSet().next() ){

                               motorista.setCnh(dBmotorista.getResultSet().getString("cnh") );
                               motorista.setDatacontratacao(dBmotorista.getResultSet().getString("datacontratacao"));
                               motorista.setIdcampusorigem(Integer.parseInt(dBmotorista.getResultSet().getString("idcampusorigem")));
                               motorista.setIdmotorista(Integer.parseInt(dBmotorista.getResultSet().getString("idmotorista")));
                               motorista.setIdusuario(Integer.parseInt(dBmotorista.getResultSet().getString("idusuario")));
                               System.out.println(motorista.getCnh());

                               dBmotorista.closedConnection();
                            }
                       }catch (SQLException ex) {
                           System.out.println(ex);
                           Logger.getLogger(Motorista.class.getName()).log(Level.SEVERE, null, ex);
                       }     
                        return motorista;
                   }else{
                       dBmotorista.closedConnection();
                   return null;                
                   }

                   }           

            }  




