/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dados.DBUsuario;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.regex.*;

/**
 *
 * @author Matheus
 */
public class Usuario {
    static private int idusuario;
    private String login;
    private String senha;
    private String nome;
    private String cpf;
    private String email;
    private int permisao;
    private String dataNasc;
    private int campusOrigem;
    static DBUsuario dbUsuario = new DBUsuario();
    
    public Usuario(){}
    
    public boolean verificaCPF() {
		return false;
}
	 
	
	 /**
          * Metódo responsável por verificar se o email que está sendo adicionado no sistema é váido.
          * @param email
          * @return boolean
          */
	public static boolean verificaEmail(String email) {  
         Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$"); 
          Matcher m = p.matcher(email); 
          if (m.find()){
            //System.out.println("O email "+email+" e valido");
            return true;
          }
          else{
           // System.out.println("O E-mail "+email+" é inválido");
            return false;
          }  

	}
	 
	public static boolean verificaSenha(String senha) {
            int h = 0;
            char minusculas[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x',
			'z', 'w', 'y' };
	char maiusculas[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X',
			'Z', 'W', 'Y' };
	char numeros[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	char vetorCaracteres[] = { '%', ',', '<', '.', '>', ';', ':', '/', ']',
			'}', '~', '^', '+', '´', '`', '[', '{', '=', '§', '-', '_', ')',
			'(', '*', '&', '¨', '$', '#', '@', '!', '¢', '¬', '?' };
	char vetorSenha[];
	char vetorvariavel[];
	String variavelcomparada = "";
        boolean num= false, maiu=false,minu=false,car =false;

			
			vetorSenha = senha.toCharArray();
			h = vetorSenha.length;
                        if (h<6 || h>10) {
                           return false; 
                        }
			for (int i = 0; i < vetorSenha.length; i++) {
				if (vetorSenha[i] == vetorSenha[i]) {

					for (int v = 0; v < numeros.length; v++) {
						if (vetorSenha[i] == numeros[v]) {
							num = true;
						}
					}
					for (int v = 0; v < minusculas.length; v++) {
						if (vetorSenha[i] == minusculas[v]) {
							minu = true;

						}
					}
					for (int v = 0; v < maiusculas.length; v++) {
						if (vetorSenha[i] == maiusculas[v]) {
							maiu = true;
						}
					}
					for (int v = 0; v < vetorCaracteres.length; v++) {
						if (vetorSenha[i] == vetorCaracteres[v]) {
							car = true;
						}
					}

				}
            
	}
       if (car && minu && maiu && num) {
                    return true;
                } else {
                    return false;
                }                 
       }
	 
    
    
	public static ResultSet verificaNomeUsuario(String nome) {
            
            return dbUsuario.listar("nomeUsuario = '"+nome+"'");
	}
    
    public boolean inserirUsuario(){
       
        dbUsuario.insert(getNome(), getLogin(), getSenha(), getEmail(), getIdusuario(), getSenha(), getCpf());
              
        //tenque tratar o retorno
       
       //dbUsuario.closedConnection();
       return true;
        
    }
    public boolean deletarUsuario(){
        DBUsuario dbUsuario = new DBUsuario();

      //  dbUsuario.delete(getIdusuario());
      
        
        //      dbUsuario.closedConnection();
       return true;
    }
    public boolean listarUsuario(){
        DBUsuario dBUsuario = new DBUsuario();

        
        try{
        
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

        
//         dBUsuario.listar(usuarios);
         
         System.out.println(usuarios.size());
        for (Usuario uses : usuarios){
            
        System.out.print(" USUARIO: "+uses.getNome());
        System.out.print(" CPF: "+uses.getCpf());
        System.out.print(" E-MAIL: "+uses.getEmail());
        System.out.print(" LOGIN: "+uses.getLogin());
        System.out.print(" SENHA: "+uses.getSenha());
        System.out.print(" ID_USUARIO: "+uses.getIdusuario());
        System.out.print(" PERMISAO: "+uses.getPermisao());
        System.out.print("\n");
        }
        return true;
               
        }catch(Exception ex){
            System.out.print("NÃO FOI POSSIVEL EXECUTAR A LISTAGEM " + ex);          
       
        }

//               dBUsuario.closedConnection();
               return false;
    }
    
        
        
   
    
    public boolean alterarUsuario(){
                DBUsuario dbUsuario = new DBUsuario();
                dbUsuario.update(getNome(), getLogin(), getSenha(), getEmail(), getCampusOrigem(), getDataNasc());
//                dbUsuario.closedConnection();
       return true;
        
    }
    
	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}
	public int getIdusuario() {
		return idusuario;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getLogin() {
		return login;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getSenha() {
		return senha;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCpf() {
		return cpf;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setPermisao(int permisao) {
		this.permisao = permisao;
	}
	public int getPermisao() {
		return permisao;
	}
	public void setDataNasc(String dataNasc) {
		this.dataNasc = dataNasc;
	}
	public String getDataNasc() {
		return dataNasc;
	}
	public void setCampusOrigem(int campusOrigem) {
		this.campusOrigem = campusOrigem;
	}
	public int getCampusOrigem() {
		return campusOrigem;
	}
    
}
