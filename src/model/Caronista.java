package model;

import dados.ConectaBD;
import dados.DBUsuario;
import dados.DBcaronista;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author matheus
 * 
 */
public class Caronista extends Usuario{
	
	private String matricula;
        private int idUsuario;
	ConectaBD con = new ConectaBD();
        static DBUsuario dbUsuario = new DBUsuario();
        static DBcaronista dbcaronista = new DBcaronista();
        Usuario user = new Usuario();
        static private ResultSet resultSet;
        static private ResultSet resultSetCar;
        
	public Caronista() {
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * Este metodo tem por finalidade buscas um caronista no sistema e caso
	 * todas as validações forem corretas exlui o caronista do sistema.
	 * 
	 * @param caronista
	 * @return boolean
         * 
	 */
	private boolean excluirCaronista(Caronista caronista) {
            
		return false;

	}
        
        /**
         * Este metodo tem por finalidade cadastrar um novo caronista no sistema
         * @param nome
         * @param matricula
         * @param senha
         * @param email
         * @param campusOrigem
         * @param dataNascimento
         * @param cpf
         * @return boolean
         */
        public static boolean incluirCaronista(String nomeCompleto,String nomeUser, String matricula,
			String senha, String email, int campusOrigem,
			String dataNascimento, String cpf) throws ParseException, SQLException {
            boolean aux = true, insertUsuario = false, insertCaronista = false;
            
            
            ValidaCPF validaCpf = new ValidaCPF();//verificação de cpf
            if (!validaCpf.isCPF(cpf)) {
                System.out.printf("CPF não é válido");
                aux = false;
            }
            if (!Caronista.validaatricula(matricula)){//verificação de matricula valida
                System.out.printf("matricula não é válida");
                aux = false;
            }
            if (!Caronista.verificarDataNasc(dataNascimento)) {//verificação de data de nascimento válida
                System.out.printf("Data de nascimento não é válida");
                aux = false;
            }
            
            if (!dbcaronista.verificaCampusValido(campusOrigem).next()) {//verificação de campus de origem válido 
               System.out.println("Campus não é válido");
               aux = false;
            }
            
            if (!Usuario.verificaEmail(email)) {//verificação de email válido
                System.out.printf("email não é válido");
              aux = false;
            }
            
            if (!Usuario.verificaSenha(senha)) {//verificar senha forte
                System.out.println("Senha não é forte");
                aux = false;
            }
            
            
            if (Usuario.verificaNomeUsuario(nomeUser).next()){//verificação de nome de usuário existente.
               System.out.println("Usuário já existe");
               aux = false;
            }
            
            if (aux) {              
                
               insertUsuario = dbUsuario.insert(nomeCompleto, nomeUser, senha, email, campusOrigem, dataNascimento, cpf);
                
                if (insertUsuario) {                    
                  insertCaronista =  dbcaronista.insert(matricula);  
                } 
   
            }
            /**
             * se for incluido um usuario  mais não for incluso um caronista ligado a este usuario, deleta o usuario inserido
             */
            if (!insertCaronista && insertUsuario) { 
              dbUsuario.delete("usuario.cpf = '"+cpf+"'");
              System.out.println("Deu errado");
            }
            
            return aux;
        }

	/**
	 * Este método tem por finalidade alterar um caronista já existente no
	 * sistema, isto ocorrerá caso todas as restrições do método forem
	 * válidadas.
	 * 
	 * @param nome
	 * @param matricula
	 * @param senha
	 * @param email
	 * @param campusOrigem
	 * @param dataNascimento
	 * @return boolean
	 */
	public static boolean alterarCaronista(String nome,String nomeUser, String matricula,String senha, String email, int campusOrigem, String dataNascimento, int idUsuario) throws ParseException, SQLException {
            
            boolean aux = true;
            
            if (!verficaUsuarioVálido(idUsuario).next()) {
                System.out.println("Usuário não tem autorização!");
                return false;
            }
            if (!Caronista.validaatricula(matricula)){//verificação de matricula valida
                System.out.println("matricula inválida - alterar");
                aux = false;
            }
            if (!Caronista.verificarDataNasc(dataNascimento)) {//verificação de data de nascimento válida
                System.out.println("Data nasc invalido - alterar");
                aux = false;
            }
            if (!dbcaronista.verificaCampusValido(campusOrigem).next()) {//verificação de campus de origem válido 
                System.out.println("Campus invalido - alterar");
              aux = false;
            }
            if (!Usuario.verificaEmail(email)) {//verificação de email válido
                System.out.println("Email alterar invalido");
              aux = false;
            }
            
            if (!Usuario.verificaSenha(senha)) {//verificar senha forte
                System.out.println("Senha não é forte");
                aux = false;
            }
            if (aux) {
              return dbcaronista.update(nome,nomeUser, senha, email, matricula,campusOrigem,dataNascimento, idUsuario);
            }
            else {
              return false;
            }
	}
        public static ResultSet verficaUsuarioVálido(int usuarioId) {
            
            return dbcaronista.verificaUsuarioProprio(usuarioId);
        }

	private boolean cancelarCarona(Caronista caronista, ViagemNormal viagem) {
		return false;
	}

        public static boolean listarCaronista(int campoOrigem) throws SQLException {
            
            String where = "campusOrigem = "+campoOrigem;
            Caronista user = new Caronista();
            String whereCar = null;
            int count = 1;
            boolean aux1 = false;
            //nome completo, e-mail, CPF, matrícula, campusdeorigem, data de nascimento
            resultSet = dbUsuario.listar(where);
            
            while (resultSet.next()) {
              user.setCpf(resultSet.getString("cpf"));
              user.setEmail(resultSet.getString("email"));
              user.setIdusuario(Integer.parseInt(resultSet.getString("idusuario")));
              user.setLogin(resultSet.getString("nomeusuario"));
              user.setNome(resultSet.getString("nome"));
              user.setSenha(resultSet.getString("senha"));
              user.setDataNasc(resultSet.getString("datanascimento"));
              user.setCampusOrigem(Integer.parseInt(resultSet.getString("campusorigem")));
              System.out.println("*************** Dados do Caronista número "+count+"**********");
            
              System.out.println("nome : "+user.getNome());
              System.out.println("email : "+user.getEmail());
              System.out.println("Camous Origem : "+user.getCampusOrigem());
              System.out.println("Data de Nascimento : "+user.getDataNasc());
              //System.out.println("MATRICULA : "+user.getMatricula());
              whereCar = "usuarioId = "+user.getIdusuario();
              resultSetCar = dbcaronista.select(whereCar);
            
                while (resultSetCar.next()) {                    
                    user.setMatricula(resultSetCar.getString("matricula"));
                }
            System.out.println("MATRICULA : "+user.getMatricula());
            System.out.println("********** FIM CARONISTA NUMERO "+count+"********************\n\n");
            
            count++;
            aux1 = true;
            }
            
            
            return aux1;
        }
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMatricula() {
		return matricula;
	}
        /**
         * Metodo que tem por objetivo validar a matricula do usuário que está se cadastrando no sistema
         * @param texto
         * @return 
         */
        public static boolean validaatricula(String texto) { 
              if(texto == null){
                return false;
              }
            for (char letra : texto.toCharArray()){
               if (letra < '0' || letra > '9')  {
                 return false;  
               }
            
            }
        return true;
        }
        
        private static boolean verificarDataNasc(String dataNascimento) throws ParseException {
        
        Calendar dataNasc=Calendar.getInstance();  
        dataNasc.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento));  
        dataNasc.add(Calendar.YEAR, 18);  
        Calendar dataAtual=Calendar.getInstance();  
        if (dataNasc.before(dataAtual)){  
          return true;
        }else{  
          return false;
       }
    }
        
	/**
     *
     * @param caronista
     * @param normal
     * @return
     */
    public boolean solicitarViagemNormal(Caronista caronista, ViagemNormal normal) {
		return false;
	}
	 
	/**
     *
     * @param data
     * @param hora
     */
        public void buscarCarona(String data, String hora) {
	 
        
	}
	 
	public boolean alterarHorarioViagemExistente() {
		return false;
	}
        public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
        
}